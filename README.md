# STEP BY STEP TO RUN THE PROJECT
#### You must have the following tools installed:
- git (last version)
- node.js (last version)
- cypress (last version)

#### Download the project from gitlab:
- `git clone https://gitlab.com/rsbarroco/ecoportal-qa-assessment.git`

#### Move to the project directory:
- `cd ecoportal-qa-assessment`

#### Checkout to Master:
- `git checkout master`

#### Run cypress 
- `./node_modules/.bin/cypress open`

#### Running E2E Testing on cypress
- Choose E2E Testing
- Choose a Browser: Chrome
- Start E2E Testing in Chrome
- Click "spec.cy.js"
